* tiaf

version 0.1.0-gimel-rc

** intro

Blockchains attract notoriously sketchy behavior. This is intended to
open up a space where 'coin' does not apply and interesting concepts
can be developed without regard to direct financial incentives or
cryptocurrency.


So, `tiaf` is a blockchain implementation designed for several purposes.

Blockchains as linear merkle trees are an interesting data
structure. Note that they actually are forming a degenerate tree form!
Having the ability to have a _node_ join a consensus database, gain
information, and continue working, is a very useful property!

Then, too, exploring the applications of a non-coin blockchain is
interesting!


Notably, similar tools exist in the for-pay space:

- https://www.bigchaindb.com/
- https://postchain-docs.readthedocs.io/en/latest/
- https://www.chainifydb.com/
- https://www.vechain.org/

An open source distributed ledger (Hyperledger) exists. "it seems
complicated" to put it mildly. Too, blockchain the data structure is
interesting.


So. `tiaf` is different.

tiaf is designed to be deployed as a trustful distributed
datastore. The authority to mint a new block or perform a new
transaction is granted by an exterior authority.

** Justification

The author of `tiaf` comes from a conventional SRE / CS major
corporation background, and considers that, for work worth something,
trust is _required_. Consider a Kubernetes deployment: the SREs of
this system have nearly perfect control of the entire enterprise. They
will deploy tools such as Consul or Linkerd to enforce peer to peer
authority, and work in conjunction with IT and application developers
to ensure that only authorized individuals can access the controlling
routes.


** Algorithms

New blocks in tiaf may be created by an API call. The caller is
presumed to have authorization.


Transactions as implemented in bitcoin with the 'mempool' are derived
from the requirement of "proof of work/stake", where a miner has to
put in time.

Design-wise, that's essentially a FIFO-ish data structure of
non-commmited writes that is shared across all peers.

This, however, is unrelated to proofs for mining, and thus can be
implemented as, suprisingly, a FIFO queue. tiaf has done so; the
contents of the queue are broadcast on reception. Double entry is
forbidden by hashing the inputs and maintaining a record of what's
already been seen in the chain and the current mempool.


** Routes

**(TODO: verify these - code is fully rewritten since -bet edition)**

REST api routes in (very draft) state. HTTP verbs to be added, which
will simplify the endpoints.

|----------+------+---------------|
| ui route | verb | description   |
|----------+------+---------------|
| /        | GET  | stab at JS ui |
|----------+------+---------------|


|-----------------------+-----------+--------------------------------------------------------------------------+---|
| API route             | HTTP verb | description                                                              |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /api/statistics       | GET       | human readable stats                                                     |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /healthz              | GET       | is the http server responding? note, does not indicate logical readiness |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /api/chain/get        | GET       | gets whole chain                                                         |   |
| /api/chain/append     | PUT       | uploads candidate chain                                                  |   |
| /api/chain/compare    | POST      | validates candidate chain                                                |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /api/block            | PUT       | adds data and commits a block                                            |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /api/record           | PUT       | adds data to the mempool, which also triggers a broadcast to peers       |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /api/peers            | PUT       | sets a peer list to pull chains from                                     |   |
| /api/peers            | GET       | retrieves the currently set peer list                                    |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|
| /api/peers/sweep      | POST      | sweeps the set peers to get their chains                                 |   |
| /api/peers/sweep/auto | PUT       | enables autosweeping peers                                               |   |
| /api/peers/sweep/auto | DELETE    | disables autosweeping peers                                              |   |
|-----------------------+-----------+--------------------------------------------------------------------------+---|


** Further work

   - Make tiaf queryable
   - Spill large chains to disk
   - Avoid sharing the entire chain when comparing lengths.

** deployment

There's a fairly hax makefile and Pulumi configuration for kubernetes. It's very prototype.

** Naming

tiaf: fiat, backwards, and misspelled.

it's not commodity money, it's not cryptocurrency. Calling it a currency is
*not even backwards*.


taif is a city in Arabia, and looks beautiful: no relation.

** Licensing

tiaf is AGPL3.  This means that if you deploy this and it
interacts in *any fashion* with a third party (not your corporation),
you must offer the third party tiaf's code, *plus* all your
modifications: AGPL3 is viral.

**BUT - ** If you want to deploy this commercially and lose the license, along
with, optionally, a support contract, contact me and we will arrange a
commercial license in conformance with your purchasing department's
policies.
